import React, { Component } from 'react';

class DateLabel extends Component {

  render() {
    let { date } = this.props;
    if ( date ) {
      return (
        <div className="date-label">
          <p> { date } </p>
        </div>
      );
    }
  }
}

export default DateLabel;

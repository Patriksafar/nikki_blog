import React, { Component } from 'react';
import DateLabel from './DateLabel';
import ArticleList from './ArticleList';

const API = "http://api.slaskouniki.cz/article/";

class Blog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null
    }
  }

  componentDidMount() {
    fetch(API + this.getUidOfArticle())
    .then(response => response.json())
    .then(data => this.setState({ data }));
  }

  render() {
    return (
      <div className="blog">
        <div className="blog__header">
          <img src="https://img.jakpost.net/c/2018/02/01/2018_02_01_39875_1517462499._large.jpg" alt = "uvodni obrazek" />
        </div>
        <div className="slider">
          <ArticleList />
        </div>
        <div className="container">
          { this.renderArticle() }
        </div>
      </div>
    );
  }

  renderArticle() {
    let { data } =  this.state;

    if (data && data !== null && data.length > 0) {
      let article = data[0];
      return (
        <div className="article--content">
          { this.renderTitle(article) }
          { this.renderDateTag(article) }
          { this.renderArticleContent(article) }
        </div>
      );
    }
  }

  renderTitle (article) {
    let { title } = article;
    if (title && title !== '') {
      return (
        <h1 className="article--title">{title}</h1>
      );
    }
  }

  renderArticleContent (article) {
    let { content } = article;
    return (
      <div className="article--text">
        <p dangerouslySetInnerHTML={{__html: content}} />
      </div>
    )
  }

  renderDateTag (article) {
    if ( article ) {
      return (
        <DateLabel date= { article.create_date.date } />
      )
    }
  }

  parseUrl(props) {
    let Url = require('url-parse');
    let url = new Url(window.location);
    return url[props];
  }

  getUidOfArticle() {
    let params = this.parseUrl('pathname').split('-');
    let delkaPole = params.length;
    let currentUid = params[delkaPole-1];

    return currentUid;
  }
}

export default Blog;

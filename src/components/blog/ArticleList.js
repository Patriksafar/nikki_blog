import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

const API = "http://api.slaskouniki.cz/article";

class ArticleList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: null
    }
  }

  componentDidMount() {
    fetch(API)
    .then(response => response.json())
    .then(data => this.setState({ data }));
  }

  render() {
    return (
      <div className="article-list">
        { this.renderArticles() }
      </div>
    );
  }

  renderArticles() {
    let { data } =  this.state;

    if (data && data !== null && data.length > 0) {
      return data.map((key, i) => {
        return (
          <div className="article-list__card" key = {i}>
            <a href= { this.create_link(key) } >
            <div className="article-list__card-image">
              <img src="https://img.jakpost.net/c/2018/02/01/2018_02_01_39875_1517462499._large.jpg" alt= {key.title} />
            </div>
            <h2 className="article-list__headline"> { key.title } </h2>
            </a>
          </div>
        )
      });
    }
  }

  create_link(article) {
    return ( "/blog/clanek-" + article.id );
  }
}

export default ArticleList;

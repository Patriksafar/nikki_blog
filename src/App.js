import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import Home from './components/home/Home';
import Blog from './components/blog/Blog';
import Error from './components/error-page/Error';
import Navigation from './components/navigation/Navigation';


class App extends Component {
  render() {
    return (
      this.router()
    );
  }

  router() {
    return (
      <BrowserRouter>
      <div>
        <Navigation />
        <Switch>
          <Route path='/' component={Home} exact />
          <Route path='/blog' component={Blog} />
          <Route component={Error} />
        </Switch>
      </div>
      </BrowserRouter>
    );
  }
}

export default App;
